package com.docql.document

import groovy.transform.TypeChecked


class MigrationException extends RuntimeException {
	
	MigrationException(String message){
		super(message)
	}
}
