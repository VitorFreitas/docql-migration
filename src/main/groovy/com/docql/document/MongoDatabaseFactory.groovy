package com.docql.document

import com.docql.Config
import com.mongodb.DB
import com.mongodb.MongoClient

class MongoDatabaseFactory {

	DB getDB(){
		try {
			DB db = new MongoClient(Config.get("mongo.url") , new Integer(Config..get("mongo.port"))).getDB(Config.get("mongo.dbname"))
			db.authenticate(Config..get("mongo.username") , Config.get("mongo.password.toCharArray()"))
			return db
		} catch(Exception ex){
			throw new MigrationException("Auth error: ${Config.get('mongo.username')} - ${ex.getMessage()}")
		}
	}
	
	def checkDB() {
		getDB().isAuthenticated()
	}
}
