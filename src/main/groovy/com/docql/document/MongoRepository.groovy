package com.docql.document

import groovy.json.JsonBuilder
import com.docql.*

import groovy.transform.TypeChecked

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.mongodb.BasicDBObject
import com.mongodb.DB
import com.mongodb.DBCollection
import com.mongodb.DBObject
import com.mongodb.util.JSON


class MongoRepository {

	DBCollection collection
	
	Logger log = LoggerFactory.getLogger(MongoRepository.class)
	
	DB db = new MongoDatabaseFactory().getDB()
	
	public MongoRepository(collection){
		this.collection = db.getCollection(collection)
	}
	
	public void checkIntegrity(){
		checkForCommited()
		clean()
	}
	
	def checkForCommited = {
		def commitedDoc = collection.find(getStatus(TransactionStatus.COMMITED))
		if (commitedDoc?.hasNext()) {
			throw new IllegalStateException("Documento de id : ${commitedDoc.next().get('_id')} com status COMMITED encontrado em colecao ${collection.getName()}")
		}
	}
	
	
	def clean = {
		try {
			remove(TransactionStatus.PENDING)
		} catch (Exception e) {
			log.warn("Documento(s) com status pending encontrados na base ${collection.name}")
		}
	}

	
	@TypeChecked
	public void insertAll(List objects){
		for(object in objects){
			insert(object)
		}
		
		doTransaction()
	}
	
	public void insert(object) {
		try{
			collection.insert(dbObject(object))
		}catch(Exception e){
			throw new MigrationException("Erro ao tentar inserir documento no banco - $e.getMessage()")
		}
	}
	
	def getPropertiesFrom(obj) {
		obj.metaClass.properties.findAll { it.name != 'class' && it.name != 'metaClass' }.inject([:]) { acc, e -> acc[e.name] = e.getProperty(obj); acc }
	  }
	
	DBObject dbObject(object) {
		def json = new JsonBuilder( getPropertiesFrom(object) + [transactionStatus : TransactionStatus.PENDING]).toString()
		(DBObject) JSON.parse(json)
	}

	void toCommited() {
		updateStatus(TransactionStatus.PENDING , TransactionStatus.COMMITED)
	}

	void toDone() {
		updateStatus(TransactionStatus.COMMITED , TransactionStatus.DONE)
	}
	
	void doTransaction(){
		toCommited()
		removeDoneOnes()
		toDone()
	} 
	
	private void updateStatus(from , to){
		try {
			collection.update(getStatus(from), new BasicDBObject("\$set", getStatus(to)), false, true )
		} catch (Exception e) {
			throw new MigrationException("Erro ao na colecao ${collection.getName()} ao tentar atualizar status de documento, de ${from} para ${to}  - $e.getMetaClass()") 
		}
	}
	
	private DBObject getStatus(TransactionStatus status){
		new BasicDBObject("transactionStatus", status.getName())
	}

	private void remove(status){
		try {
			collection.remove(getStatus(status))
		} catch (Exception e) {
			throw new MigrationException("Erro ao tentar remover documentos de status $status - $e.getMessage()")
		}
	} 
	
	void removeDoneOnes() {
		remove(TransactionStatus.DONE)
	}

	void removePendingOnes() {
		remove(TransactionStatus.PENDING)
	}
}