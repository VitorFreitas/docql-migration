package com.docql.meta;



class ReflectionProcessor {

	 def getClasses(String packageName) {
		def classLoader = Thread.currentThread().getContextClassLoader();				
		assert classLoader != null;
		Enumeration<URL> resources = classLoader.getResources(packageName.replace('.', '/'));
		List<File> dirs = new ArrayList<File>();
		
		while (resources.hasMoreElements())   {
			URL resource = resources.nextElement();
			dirs.add(new File(resource.getFile()));
		}
		
		def classes = []
		for (File directory : dirs) {
			classes.addAll(findClasses(directory, packageName));
		}
		
		return classes
	}

	def findClasses(File directory, String packageName) throws ClassNotFoundException {
		List<Class> classes = new ArrayList<Class>();
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				assert !file.getName().contains(".");
				classes.addAll(findClasses(file, packageName + "." + file.getName()));
			} else if (file.getName().endsWith(".class")) {
				classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
			}
		}
		return classes;
	}
}
