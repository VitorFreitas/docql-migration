package com.docql

import groovy.json.JsonBuilder

abstract class MongoObject {
	
	String transactionStatus = TransactionStatus.PENDING.getName()
	
	final String toJsonString(){
		new JsonBuilder(getMongoProperties() + [transactionStatus : transactionStatus]).toString()
	}
	
	abstract java.util.Map<String , Object> getMongoProperties() 
}
