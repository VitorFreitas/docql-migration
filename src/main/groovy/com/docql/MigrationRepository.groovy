package com.docql

import groovy.transform.TypeChecked

import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.docql.document.MigrationException
import com.docql.document.MongoRepository
import com.docql.relational.DatabaseConnectionFactory

abstract class MigrationRepository {

	@Delegate 
	MongoRepository mongoRepository
	
	Logger log = LoggerFactory.getLogger(MigrationRepository.class)
	
	private String collection
	
	MigrationRepository(String mongoCollectionName){
		collection = mongoCollectionName
		mongoRepository = new MongoRepository(collection)
		mongoRepository.checkIntegrity()
	}
	
	def getStatment(){
		DatabaseConnectionFactory.getStatment()
	}
	
	@TypeChecked
	List findAll() {
		try {
			executeQuery()
		} catch (Exception e) {
			throw new MigrationException("Erro ao executar query na tabela \n query : ${query()} - ${e.getMessage()}")
		}
	}

	private List executeQuery() {
		List results = []		
		ResultSet resultSet = statment.executeQuery(query())
		while(resultSet.next())	results << populate(resultSet) 		
		results
	}
	    
	public int priority(){
		0
	}
	
	final void migrate(){
		List objects = findAll()		
		log.info("Migrando ${objects.size()} objetos para colecao ${collection}")
		mongoRepository.insertAll(objects)
		objects.clear()
		log.info("Migracao de ${collection} realizada com sucesso")
	}
	
	protected Connection getConnection(){
		DatabaseConnectionFactory.connection
	}
	
	
	protected List listFromQuery(String query , Object... parameters , PopulateTemplate populate){		
		PreparedStatement prepareStatement = DatabaseConnectionFactory.getConnection().prepareStatement(query);
		parameters.eachWithIndex { object , index ->
			prepareStatement.setObject(index + 1, object)
		}	
		ResultSet resultSet = prepareStatement.executeQuery();
		List results = []
		
		while(resultSet.next()){
			results << populate.populateList(resultSet);
		}
		return results;
	}
	
	abstract String query()

	abstract Object populate(ResultSet resultSet)
}