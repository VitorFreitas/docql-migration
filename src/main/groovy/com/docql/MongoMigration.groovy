package com.docql

import com.docql.document.MigrationException
import com.docql.meta.ReflectionProcessor


class MongoMigration {

	List repositories

	//Logger log = LoggerFactory.getLogger(MongoMigration.class)
		
	MongoMigration(){
		this.repositories = createRepositoryInstances()
		//this.repositories = new JsonMigrationFlow().getInstances()
	}
	
	void migrateAll(){
		try {
	//		log.info("Iniciando migracao de dados")
		//	log.info("Repositorios encontrados: ${repositories.collect({ it.class.getSimpleName() })}")
		//	verifyConnections()
			//verifyIntegrity()
			repositories.each { MigrationRepository repository ->
				repository.migrate()
			}
			//log.info("Migracao realizada com sucesso")
		} catch (Exception e) {
			//log.error("Erro na migracao : ${e.getMessage()}")
		}
	}
	
	private void verifyConnections(){
		try{
			new Config().checkConnections()
		}catch(Exception e){
			throw new MigrationException("Erro ao fazer conexoes - ${e.getMessage()}")
		}
	}
	
	private void verifyIntegrity() { 
		repositories.each { MigrationRepository repository ->
			repository.checkIntegrity()
		}
	}
	
	public static void main(String[] args) {
		try {
			new MongoMigration().migrateAll()
		} catch (Exception e) {
			println e
			throw e//new RuntimeException("Erro na migracao: ${e.getMessage()}")
		}
	}
	
	private List<? extends MigrationRepository> createRepositoryInstances() {
		new ReflectionProcessor(System.getProperty("scanPath")).getInstances()
	}

}