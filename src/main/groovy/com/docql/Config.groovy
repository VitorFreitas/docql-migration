package com.docql

import com.docql.document.MongoDatabaseFactory
import com.docql.relational.DatabaseConnectionFactory


class Config {

	static ConfigObject configs = getConfigFromProperties()
	
	static {
		configs = getConfigFromProperties()				
		configs.toProperties.stringPropertyNames().each {
			Config.metaClass.static."$it" = configs."$it"
		}
	}		
	
	private static ConfigObject getConfigFromProperties(){
		Properties properties = new Properties()
		properties.load(new FileInputStream("config.properties"))
		new ConfigSlurper().parse(properties)
	}
	
	static String get(key){
		configs.toProperties().getProperty(key)
	} 
}