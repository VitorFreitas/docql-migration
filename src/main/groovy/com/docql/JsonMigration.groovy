package com.docql

import java.sql.ResultSet;

class JsonMigration extends MigrationRepository {

	def migrationObj
	
	JsonMigration(migrationObj){
		super("testeeeee");
		this.migrationObj = migrationObj
	}
	
	@Override
	 String query() {
		return migrationObj.query
	}

	@Override
	public Object populate(ResultSet resultSet) {		
		def exp = new Expando()
		def meta = resultSet.getMetaData()
		for (int i = 1; i < meta.getColumnCount() + 1; i++) {
			exp."${meta.getColumnName(i)}" = resultSet.getString(i)	 
		}
		return exp
	}


}