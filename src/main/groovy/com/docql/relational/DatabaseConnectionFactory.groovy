package com.docql.relational;

import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.sql.Statement

import com.docql.Config
import com.docql.document.MigrationException

class DatabaseConnectionFactory {

	static {
		try {
			Class.forName(Config.get("database.driverClass"));
		} catch (SQLException e) {
			throw new MigrationException("Driver not found - ${e.getMessage()}" , e) 
		}
	}
	
	static Statement getStatment() throws SQLException {
		getConnection().createStatement()
	}
	
	static Connection getConnection(){
		DriverManager.getConnection(Config.get("database.url"),Config.get("database.username"), Config.get("database.password"))
	}
}