package com.migrationapi;

import static org.junit.Assert.*


public class MongoFreightRepositoryTest{
	
/*	String COLLECTION_NAME = "teste_migracao";

	DB db;
	DBCollection dbCollection;
	MongodExecutable mongodExe;
	Mongo mongo;
		
	@Before
	void startResources() {
		startEmbeddedMongoDB();
        mongo = new Mongo("localhost", 12345);
        db = mongo.getDB( "test_migration" );
        dbCollection = db.getCollection( "test_freight" );

        new MigrationConfig().checkConnections();
		DB db = new MongoDatabaseFactory().getDB();
		dbCollection.drop();
	}

	private void startEmbeddedMongoDB()  {
		MongodStarter runtime = MongodStarter.getDefaultInstance();
		mongodExe = runtime.prepare(new MongodConfig(Version.Main.V2_0, 12345, Network.localhostIsIPv6()));
		mongodExe.start();
	}
	
	@After
	void stopMongo(){
		dbCollection.drop();
		mongodExe.stop();
	}
	
	@Test
	public void test_insere_preco_no_mongo() {
		MongoObject object = createMongoObject();
		new MongoRepository(COLLECTION_NAME).insert(object);
		assertNotNull(dbCollection.findOne());
		assertEquals(1 , new Integer(dbCollection.findOne().get("value1").toString()).intValue());
		
	}

	private MongoObject createMongoObject() {
		return new MongoObject() {
			
			@Override
			public Map<String, Object> getMongoProperties() {
				Map value3 = new HashMap<String, String>();
				value3.put("value3_1", "3");
				value3.put("value3_2", "3");
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("value1", 1);
				map.put("value2", "string");
				map.put("value3", value3);
				return map;
			}
		};
	}

	
	public void test_insere_status_de_pedentes() {
		new MongoRepository(COLLECTION_NAME).insert(createMongoObject());
		assertEquals(TransactionStatus.PENDING.getName(), dbCollection.findOne().get("transactionStatus"));
	}
	
	public void test_insere_atualiza_pedentes_para_commited() {
		MongoRepository repository = new MongoRepository(COLLECTION_NAME);
		repository.insert(createMongoObject());
		repository.insert(createMongoObject());
		
		assertEquals(TransactionStatus.PENDING.getName(), dbCollection.findOne().get("transactionStatus"));
		
		repository.toCommited();
		
		assertEquals(TransactionStatus.COMMITED.getName(), dbCollection.findOne().get("transactionStatus"));
		
		assertEquals(TransactionStatus.COMMITED.getName(), dbCollection.find().skip(1).next().get("transactionStatus"));
	}

	
	public void test_insere_atualiza_commited_para_done() {
		MongoRepository repository = new MongoRepository(COLLECTION_NAME);
		repository.insert(createMongoObject());
		repository.insert(createMongoObject());

		repository.toCommited();
		
		repository.toDone();
		
		assertEquals(TransactionStatus.DONE.getName(), dbCollection.findOne().get("transactionStatus"));
		assertEquals(TransactionStatus.DONE.getName(), dbCollection.find().skip(1).next().get("transactionStatus"));
	}

	public void test_remove_documentos_done() {
		MongoRepository repository = new MongoRepository(COLLECTION_NAME);
		repository.insert(createMongoObject());
		repository.insert(createMongoObject());

		repository.toCommited();
		repository.toDone();

		repository.insert(createMongoObject());
		
		repository.removeDoneOnes();

		
		assertEquals(1L, dbCollection.count());
		assertEquals(TransactionStatus.PENDING.getName() , dbCollection.findOne().get("transactionStatus"));
	}
	
	public void test_verifica_integridade_colecao() {
		MongoObject freightPrice = createMongoObject();
		freightPrice.setTransactionStatus(TransactionStatus.COMMITED.getName());

		dbCollection.insert((DBObject) JSON.parse(freightPrice.toJsonString()));
		dbCollection.insert((DBObject) JSON.parse(freightPrice.toJsonString()));
		
		try {
			MongoRepository repository = new MongoRepository(COLLECTION_NAME);
			repository.checkIntegrity();
			fail();
		} catch (IllegalStateException e) {
		}
	}


	public void test_remove_documentos_pending() {
		MongoRepository repository = new MongoRepository(COLLECTION_NAME);
		repository.insert(createMongoObject());
		repository.insert(createMongoObject());
		repository.insert(createMongoObject());

		repository.removePendingOnes();
		
		assertEquals(0L, dbCollection.count());
	}	*/
}